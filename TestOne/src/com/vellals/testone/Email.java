package com.vellals.testone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Email extends Activity implements View.OnClickListener {
	
	EditText etName, etEmail, etIntro, etLikes, etTodo, etOut;
	String sName, sEmail, sIntro, sLikes, sTodo, sOut;
	Button submit;
	TextView tvMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email);
		setupXMLReferences();
		
		submit.setOnClickListener(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		updateStrings();
		String[] emailAddresses = { sEmail };
		String msg = "Hello " + sName + ". "
				+ "I see you like " + sLikes + ". " 
				+ "You can " + sTodo + " after which we can go to "
				+ sOut + ". You can introduce yourself as "
				+ sIntro + ".";
		tvMsg.setText(msg);
		
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.putExtra(Intent.EXTRA_EMAIL, emailAddresses);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email Subject");
		emailIntent.setType("plain/text");
		emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
		startActivity(emailIntent);
	}

	protected void updateStrings() {
		// TODO Auto-generated method stub
		sName = etName.getText().toString();
		sEmail = etEmail.getText().toString();
		sIntro = etIntro.getText().toString();
		sLikes = etLikes.getText().toString();
		sTodo = etTodo.getText().toString();
		sOut = etOut.getText().toString();
	}

	private void setupXMLReferences() {
		// TODO Auto-generated method stub
		etName = (EditText) findViewById(R.id.etName);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etIntro = (EditText) findViewById(R.id.etIntro);
		etLikes = (EditText) findViewById(R.id.etLikes);
		etTodo = (EditText) findViewById(R.id.etTodo);
		etOut = (EditText) findViewById(R.id.etOut);
		submit = (Button) findViewById(R.id.btSubmit);
		tvMsg = (TextView) findViewById(R.id.tvMsg);
	}
}
