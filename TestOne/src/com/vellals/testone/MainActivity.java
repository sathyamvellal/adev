package com.vellals.testone;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.*;

public class MainActivity extends Activity {
	
	int val;
	Button bAdd;
	Button bSub;
	TextView tvVal;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        val = 0;
        bAdd = (Button) findViewById(R.id.butAdd);
        bSub = (Button) findViewById(R.id.butSub);
        tvVal = (TextView) findViewById(R.id.txtVal);
        
        bAdd.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				++val;
				tvVal.setText("" + val);
			}
		});
        bSub.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				--val;
				tvVal.setText("" + val);
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
}
