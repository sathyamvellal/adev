package com.vellals.testone;

import java.util.Random;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class TextPlay extends Activity {

	Button chkCmd;
	ToggleButton toggle;
	EditText input;
	TextView display;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text);
		
		setupXMLReferences();
		
		toggle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (toggle.isChecked() == true) {
					input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				} else {
					input.setInputType(InputType.TYPE_CLASS_TEXT); 
				}
			}
		});
		
		chkCmd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String check = input.getText().toString();
				display.setText(check);
				if (check.contentEquals("left")) {
					display.setGravity(Gravity.LEFT);
				} else if (check.contentEquals("right")) {
					display.setGravity(Gravity.RIGHT);
				} else if (check.contentEquals("center")) {
					display.setGravity(Gravity.CENTER);
				} else if (check.contentEquals("blue")) {
					display.setTextColor(Color.BLUE);
				} else if (check.contains("foo")) {
					Random rand = new Random();
					display.setText("foo: " + (rand.nextInt(100)));
					display.setTextSize(rand.nextInt(100));
					display.setTextColor(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
					switch (rand.nextInt(3)) {
					case 0:
						display.setGravity(Gravity.LEFT);
						break;
					case 1:
						display.setGravity(Gravity.CENTER);
						break;
					case 2:
						display.setGravity(Gravity.RIGHT);
						break;
					}
				} else {
					display.setText("Invalid");
					display.setGravity(Gravity.CENTER);
				}
			}
		});
	}

	private void setupXMLReferences() {
		// TODO Auto-generated method stub
		chkCmd = (Button) findViewById(R.id.btResult);
		toggle = (ToggleButton) findViewById(R.id.tbPassword);
		input = (EditText) findViewById(R.id.etCmds);
		display = (TextView) findViewById(R.id.tvResult);
	}

}
